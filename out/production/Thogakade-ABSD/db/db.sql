create table customer
(
    customer_id      int primary key AUTO_INCREMENT,
    customer_name    varchar(200),
    customer_address varchar(200),
    customer_salary  double,
    user_name varchar(200),
    password varchar(200)
);

create table item
(
    item_code        int primary key AUTO_INCREMENT,
    item_name        varchar(200),
    item_description varchar(200),
    item_price       double,
    item_qty         int
);
