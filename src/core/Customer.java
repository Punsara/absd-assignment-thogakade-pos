package core;

import java.io.Serializable;

public class Customer implements Serializable {
    private int id;
    private String customerName;
    private String customerAddress;
    private double customerSalary;
    private String customerUserName;
    private String customerPassword;

    public Customer() {}

    public Customer(int id, String customerName, String customerAddress, double customerSalary) {
        this.id = id;
        this.customerName = customerName;
        this.customerAddress = customerAddress;
        this.customerSalary = customerSalary;
    }

    public Customer(int id, String customerName, String customerAddress, double customerSalary, String customerUserName, String customerPassword) {
        this.id = id;
        this.customerName = customerName;
        this.customerAddress = customerAddress;
        this.customerSalary = customerSalary;
        this.customerUserName = customerUserName;
        this.customerPassword = customerPassword;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public double getCustomerSalary() {
        return customerSalary;
    }

    public String getCustomerUserName() {
        return customerUserName;
    }

    public void setCustomerUserName(String customerUserName) {
        this.customerUserName = customerUserName;
    }

    public String getCustomerPassword() {
        return customerPassword;
    }

    public void setCustomerPassword(String customerPassword) {
        this.customerPassword = customerPassword;
    }

    public void setCustomerSalary(double customerSalary) {
        this.customerSalary = customerSalary;
    }
}
