package listener;

import db.DBConnection;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.sql.DataSource;

public class ContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext servletContext = sce.getServletContext();
        DataSource connectionPool = DBConnection.getConnectionPool();
        servletContext.setAttribute("connectionPool", connectionPool);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
