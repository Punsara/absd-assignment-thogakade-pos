package webapp;

import core.Customer;
import db.DBConnection;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "ManageCustomer")
public class ManageCustomer extends HttpServlet {
    DataSource dataSource;

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext servletContext = getServletContext();
        if(dataSource == null){
            dataSource = (DataSource) servletContext.getAttribute("connectionPool");
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        String customerName = request.getParameter("customerName");
        String customerAddress = request.getParameter("customerAddress");
        double customerSalary = Double.parseDouble(request.getParameter("customerSalary"));
        String userName = request.getParameter("userName");
        String password = request.getParameter("password");

        Connection connection = null;
        PreparedStatement statement = null;
        try{
            DataSource pool = DBConnection.getConnectionPool();
            connection = pool.getConnection();

            statement = connection.prepareStatement("INSERT INTO customer VALUES(?,?,?,?,?,?)");
            statement.setInt(1,0);
            statement.setString(2,customerName);
            statement.setString(3,customerAddress);
            statement.setDouble(4,customerSalary);
            statement.setString(5,userName);
            statement.setString(6,password);
            int resultSet = statement.executeUpdate();
            writer.println(resultSet);
        }catch (Exception ex){
            ex.printStackTrace();
            writer.println(0);
        }finally {
            if (statement != null){
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        Connection connection=null;
        PreparedStatement statement =null;
        try {
            DataSource pool = DBConnection.getConnectionPool();
            connection = pool.getConnection();

            statement = connection.prepareStatement("SELECT * FROM customer");
            ResultSet resultSet = statement.executeQuery();
            List<Customer> customerList = new ArrayList<>();
            while (resultSet.next()){
                customerList.add(new Customer(resultSet.getInt(1),resultSet.getString(2),resultSet.getString(3),resultSet.getDouble(4)));
            }
            writer.println(customerList);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (statement != null){
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        String customerName = request.getParameter("customerName");
        String customerAddress = request.getParameter("customerAddress");
        double customerSalary = Double.parseDouble(request.getParameter("customerSalary"));


        Connection connection = null;
        PreparedStatement statement = null;
        try{
            DataSource pool = DBConnection.getConnectionPool();
            connection = pool.getConnection();

            statement = connection.prepareStatement("UPDATE customer SET customer_name = ?, customer_address = ?, customer_salary = ? WHERE customer_id = ?");
            statement.setInt(1,2);
            statement.setString(2,customerName);
            statement.setString(3,customerAddress);
            statement.setDouble(4,customerSalary);
            int resultSet = statement.executeUpdate();
            writer.println(resultSet);
        }catch (Exception ex){
            ex.printStackTrace();
            writer.println(0);
        }finally {
            if (statement != null){
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
