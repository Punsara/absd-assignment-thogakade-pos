package webapp;

import core.Item;
import db.DBConnection;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "ManageItem")
public class ManageItem extends HttpServlet {
    DataSource dataSource;

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext servletContext = getServletContext();
        if(dataSource == null){
            dataSource = (DataSource) servletContext.getAttribute("connectionPool");
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        String itemName = request.getParameter("itemName");
        String itemDescription = request.getParameter("itemDescription");
        double itemPrice = Double.parseDouble(request.getParameter("itemPrice"));
        int qtyOnHand = Integer.parseInt(request.getParameter("qtyOnHand"));

        Connection connection = null;
        PreparedStatement statement = null;
        try{
            DataSource pool = DBConnection.getConnectionPool();
            connection = pool.getConnection();

            statement = connection.prepareStatement("INSERT INTO item VALUES(?,?,?,?,?)");
            statement.setInt(1,0);
            statement.setString(2,itemName);
            statement.setString(3,itemDescription);
            statement.setDouble(4,itemPrice);
            statement.setInt(5,qtyOnHand);
            int resultSet = statement.executeUpdate();
            writer.println(resultSet);
        }catch (Exception ex){
            ex.printStackTrace();
            writer.println(0);
        }finally {
            if (statement != null){
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        Connection connection=null;
        PreparedStatement statement =null;
        try {
            DataSource pool = DBConnection.getConnectionPool();
            connection = pool.getConnection();

            statement = connection.prepareStatement("SELECT * FROM item");
            ResultSet resultSet = statement.executeQuery();
//            List<Item> itemList = new ArrayList<>();
            StringBuffer stringBuffer = new StringBuffer("{ \"allItems\" : {");
            int i=0;
            while (resultSet.next()){
                stringBuffer.append(+i++ + ": {");
                stringBuffer.append("\"itemId\": "+resultSet.getInt(1)+",");
                stringBuffer.append("\"itemName\": \""+resultSet.getString(2)+"\",");
                stringBuffer.append("\"itemDescription\": \""+resultSet.getString(3)+"\",");
                stringBuffer.append("\"itemPrice\": "+resultSet.getDouble(4)+",");
                stringBuffer.append("\"itemQty\": "+resultSet.getInt(5)+"},");
                //itemList.add(new Item(resultSet.getInt(1),resultSet.getString(2),resultSet.getString(3),resultSet.getDouble(4),resultSet.getInt(5)));
            }
            stringBuffer.append("}}");
//            response.getWriter().write(stringBuffer.toString());
            writer.println(stringBuffer);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (statement != null){
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        String itemCode = request.getParameter("itemCode");

        String itemName = request.getParameter("itemName");
        String itemDescription = request.getParameter("itemDescription");
        double itemPrice = Double.parseDouble(request.getParameter("itemPrice"));
        int qtyOnHand = Integer.parseInt(request.getParameter("qtyOnHand"));


        Connection connection = null;
        PreparedStatement statement = null;
        try{
            DataSource pool = DBConnection.getConnectionPool();
            connection = pool.getConnection();

            statement = connection.prepareStatement("UPDATE item SET item_name = ?, item_description = ?, item_price = ?, item_qty = ? WHERE item_code = ?");
            statement.setInt(1,2);
            statement.setString(2,itemName);
            statement.setString(3,itemDescription);
            statement.setDouble(4,itemPrice);
            statement.setInt(5,qtyOnHand);
            int resultSet = statement.executeUpdate();
            writer.println(resultSet);
        }catch (Exception ex){
            ex.printStackTrace();
            writer.println(0);
        }finally {
            if (statement != null){
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
