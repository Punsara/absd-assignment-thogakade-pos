package db;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class DBConnection {
    public static DataSource getConnectionPool(){
        InitialContext initialContext = null;
        DataSource dataSource = null;

        try{
            initialContext = new InitialContext();
            dataSource = (DataSource) initialContext.lookup("java:comp/env/thogakade_pos_absd");
        }catch (NamingException ex){
            ex.printStackTrace();
        }
        return dataSource;
    }
}
